# OpenML dataset: scm1d

https://www.openml.org/d/41485

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multivariate regression data set from: https://link.springer.com/article/10.1007%2Fs10994-016-5546-z : The Supply Chain Management datasets are derived from the Trading Agent Competition in Supply Chain Management (TAC SCM) tournament from 2010. The precise methods for data preprocessing and normalization are described in detail by Groves and Gini (2011). Some benchmark values for prediction accuracy in this domain are available from the TAC SCM Prediction Challenge (Pardoe and Stone 2008), these datasets correspond only to the Product Future prediction type. Each row corresponds to an observation day in the tournament (there are 220 days in each game and 18 tournament games in a tournament). The input variables in this domain are observed prices for a specific tournament day. In addition, 4 time-delayed observations are included for each observed product and component (1, 2, 4 and 8 days delayed) to facilitate some anticipation of trends going forward. The datasets contain 16 regression targets, each target corresponds to the next day mean price (SCM1D) or mean price for 20-days in the future (SCM20D) for each product in the simulation. Days with no target values are excluded from the datasets (i.e. days with labels that are beyond the end of the game are excluded).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41485) of an [OpenML dataset](https://www.openml.org/d/41485). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41485/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41485/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41485/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

